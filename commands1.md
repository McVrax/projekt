## rebase
​
* **git rebase {nazwa gałęzi}** - dołączenie zmian do wskazanej gałęzi z gałęzi na której obecnie jesteś z zachowaniem kolejności wprowadzania zmian
* **git rebase {nazwa remota}/{nazwa gałęzi}** - dołączenie zmian ze wskazanego repozytorium i gałęzi z zachowaniem kolejności wprowadzania zmian
* **git rebase {nazwa gałęzi1} {nazwa gałęzi2}** - dołącza głąź2 do gałęzi1 z zachowaniem kolejności wprowadzonych zmian 
* **git rebase --abort** - przerywa łączenie (możliwe, gdy wystąpią konflikty)
* **git rebase --continue** - po rozwiązaniu konflitów zapisuje zmiany
* **git rebase --interactive {commit}** - pozwala wybrać commity które zostaną dołączone (lub modyfikować)
* **git rebase --interactive '{hash}^'** - umożliwia edycję commitów do podanego hasha
